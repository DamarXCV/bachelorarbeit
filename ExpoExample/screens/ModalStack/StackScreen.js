import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';

class ModalsScreen extends React.Component {
    handlePress = () => this.props.navigation.push('StackScreen');

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>Modals Screen</Text>
                <Button
                    buttonStyle={styles.button}
                    title="Go to StackScreen"
                    onPress={this.handlePress}
                />
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
    button: {
        width: 300,
        margin: 10,
    },
});

export default ModalsScreen;