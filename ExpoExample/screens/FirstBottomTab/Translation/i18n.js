import { Localization } from 'expo-localization';
import i18n from 'i18n-js';

const de = {
    "title": "Titel in deutsch",
    "current": "Die Sprache ist auf \"{{language}}\" gesetzt"
}

const en = {
    "title": "Title in english",
    "current": "The current language is \"{{language}}\""
}

i18n.locale = Localization.locale;
i18n.fallbacks = true;
i18n.translations = { en, de };

export default i18n;