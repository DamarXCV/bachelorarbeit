import React from "react";
import { View, Text, StyleSheet } from "react-native";

import i18n from './i18n';

class Translation extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>Localization Screen</Text>
                <Text>{i18n.t('title')}</Text>
                <Text>{i18n.t('current', { language: i18n.currentLocale() })}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
});

export default Translation;