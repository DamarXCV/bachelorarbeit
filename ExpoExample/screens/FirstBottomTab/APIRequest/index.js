import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';

class APIRequest extends React.Component {
    handlePressFetchAPI = () => this.props.navigation.push('FetchAPIScreen');
    handlePressAxios = () => this.props.navigation.push('AxiosScreen');

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>Home Screen</Text>
                <Button
                    buttonStyle={styles.button}
                    title="Go to Fetch API"
                    onPress={this.handlePressFetchAPI}
                />
                <Button
                    buttonStyle={styles.button}
                    title="Go to axios"
                    onPress={this.handlePressAxios}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
    button: {
        width: 300,
        margin: 10,
    },
});

export default APIRequest;