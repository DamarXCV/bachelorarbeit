import React from "react";
import { View, Text, FlatList, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';

const jsonSourceURL = "https://jsonplaceholder.typicode.com/users";

class FetchAPI extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            error: false,
            isLoading: false,
        }
    }
    
    handleFetch = (response) => {
        if (!response.ok) {
            throw Error(response.statusText);
        }
        return response.json();
    }

    getData = () => {
        this.setState((prevState) => ({
            users: [],
            isLoading: true,
            error: false,
        }));

        fetch(jsonSourceURL)
            .then(this.handleFetch)
            .then((response) => {
                this.setState((prevState) => ({
                    users: response,
                    error: false,
                    isLoading: false,
                }));
            })
            .catch((error) => {
                this.setState((prevState) => ({
                    users: [],
                    error: error.toString(),
                    isLoading: false,
                }));
            });
    }

    keyExtractor = (item, index) => item.id.toString();

    renderItem = ({ item }) => (
        <View key={item.id.toString()}>
            <Text style={{ padding: 10, fontSize: 18, height: 44 }}>
                {item.name}
            </Text>
        </View>
    )

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Button
                    buttonStyle={styles.button}
                    onPress={this.getData}
                    title="Request Data!"
                />
                {this.state.error &&
                    <Text style={{color: "red"}}>
                        {this.state.error}
                    </Text>
                }
                <FlatList
                    data={this.state.users}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem}
                    ListEmptyComponent={
                        this.state.isLoading &&
                        <Text>
                            Loading...
                        </Text>
                    }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        width: 300,
        margin: 10,
    },
});

export default FetchAPI;