import React from "react";
import { View, Text, FlatList, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';
import axios from "axios";

const CancelToken = axios.CancelToken;
const source = CancelToken.source();

const jsonSourceURL = "https://jsonplaceholder.typicode.com/users";
const mp4SourceURL = "https://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_30mb.mp4";
const config = {
    headers: {
        CacheControl : 'max-age=0',
}};
 
class Axios extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            error: false,
            isLoading: false,
            progress: 0,
        }
    }

    handleFetch = (response) => {
        if (!response.ok) {
            throw Error(response.statusText);
        }
        return response.json();
    }
    
    getData = () => {
        this.setState((prevState) => ({
            users: [],
            isLoading: true,
            error: false,
        }));

        axios.get(jsonSourceURL)
          .then((response) => {
            this.setState((prevState) => ({
                users: response.data,
                error: false,
                isLoading: true,
            }));
        })
        .catch((error) => {
            this.setState((prevState) => ({
                users: [],
                error: error.toString(),
                isLoading: true,
            }));
        });
    }

    getDataAndCancel = () => {
        this.setState((prevState) => ({
            users: [],
            isLoading: true,
            error: false,
        }));

        axios.get(jsonSourceURL, {
            cancelToken: source.token,
            ...config,
        })
          .then((response) => {
            this.setState((prevState) => ({
                users: response.data,
                error: false,
                isLoading: true,
            }));
        })
        .catch((error) => {
            this.setState((prevState) => ({
                users: [],
                error: error.toString(),
                isLoading: true,
            }));
        });

        source.cancel();
    }

    getDataAndTimeout = () => {
        this.setState((prevState) => ({
            users: [],
            isLoading: true,
            error: false,
        }));
        {/* URL needs to be different otherwise the answer gets cached and
         its works just if no previous request succeed */}
        axios.get(jsonSourceURL + '#' + new Date().getTime(), {
            timeout: 5,
            ...config,
        })
            .then((response) => {
                this.setState((prevState) => ({
                    users: response.data,
                    error: false,
                    isLoading: true,
                }));
            })
            .catch((error) => {
                this.setState((prevState) => ({
                    users: [],
                    error: error.toString(),
                    isLoading: true,
                }));
            });
    }

    getVideo = () => {
        this.setState((prevState) => ({
            isLoading: true,
            error: false,
        }));

        axios.get(mp4SourceURL, {
            onDownloadProgress: (progressEvent) => {
                this.setState((prevState) => ({
                    // not working for android https://github.com/axios/axios/issues/1832
                    progress: progressEvent.loaded,
                }));
            },
        })
        .then((response) => {
            this.setState((prevState) => ({
                error: false,
                isLoading: false,
            }));
        })
        .catch((error) => {
            this.setState((prevState) => ({
                error: error.toString(),
                isLoading: false,
            }));
        });
    }


    keyExtractor = (item, index) => item.id.toString();

    renderItem = ({ item }) => (
        <View key={item.id.toString()}>
            <Text style={{ padding: 10, fontSize: 18, height: 44 }}>
                {item.name}
            </Text>
        </View>
    )

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Button
                    buttonStyle={styles.button}
                    onPress={this.getData}
                    title="Request Data!"
                />
                <Button
                    buttonStyle={styles.button}
                    onPress={this.getDataAndCancel}
                    title="Request Data and Cancel!"
                />
                <Button
                    buttonStyle={styles.button}
                    onPress={this.getDataAndTimeout}
                    title="Request Data and Timeout!"
                />
                {/* <Button onPress={this.getVideo} title="Request Video!"/>
                <Text>
                    Progress: {this.state.progress}
                </Text> */}
                {this.state.error &&
                    <Text style={{color: "red"}}>
                        {this.state.error}
                    </Text>
                }
                <FlatList
                    data={this.state.users}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem}
                    ListEmptyComponent={
                        this.state.isLoading && !this.state.error &&
                        <Text>
                            Loading...
                        </Text>
                    }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        width: 300,
        margin: 10,
    },
});

export default Axios;