import React, { Fragment } from "react";
import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    CheckBox,
    Button,
    TextInput
} from "react-native";

import {
    Avatar as AvatarRNE,
    Badge as BadgeRNE,
    Button as ButtonRNE,
    Card as CardRNE,
    Icon as IconRNE,
    CheckBox as CheckBoxRNE,
    Input as InputRNE,
} from 'react-native-elements';

import {
    Badge as BadgeNB,
    Text as TextNB,
    Button as ButtonNB,
    DatePicker as DatePickerNB,
    Container as ContainerNB,
    Content as ContentNB,
    Card as CardNB,
    CardItem as CardItemNB,
    Body as BodyNB,
    CheckBox as CheckBoxNB,
    Input as InputNB,
    Picker as PickerNB,
    ListItem as ListItemNB,
    Icon as IconNB,
} from 'native-base';

import {
    RkBadge,
    RkButton,
    RkCalendar,
    RkCard,
    RkChoice,
    RkTextInput,
    RkPicker,
    RkText,
} from 'react-native-ui-kitten';

import {
    Avatar as AvatarRNMUI,
    Badge as BadgeRNMUI,
    Button as ButtonRNMUI,
    Card as CardRNMUI,
    Divider as DividerRNMUI,
    Icon as IconRNMUI,
    Subheader as SubheaderRNMUI,
    Checkbox as CheckboxRNMUI,
} from 'react-native-material-ui';

class UIKits extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center' }}>
                <Text>UIKits Screen</Text>
                <ScrollView>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <Fragment>
                            <SubheaderRNMUI style={stylesRNMUI.text} text="Avatars" />
                            <Text style={styles.subheader}>React Native Elements</Text>
                            <AvatarRNE
                                rounded
                                source={{
                                    uri:
                                    'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                                }}
                            />

                            <Text style={styles.subheader}>Native Base</Text>
                            <Text>not implemented</Text>

                            <Text style={styles.subheader}>UI kitten</Text>
                            <Text>not implemented</Text>

                            <Text style={styles.subheader}>React Native Material UI</Text>
                            <AvatarRNMUI
                                icon="grade"
                            />
                            <DividerRNMUI/>
                        </Fragment>
                        <Fragment>
                            <SubheaderRNMUI style={stylesRNMUI.text} text="Badge" />
                            <Text style={styles.subheader}>React Native Elements</Text>
                            <BadgeRNE value="99+" status="error" />
                            <BadgeRNE value={<Text>My Custom Badge</Text>}/>
                            
                            <Text style={styles.subheader}>Native Base</Text>
                            <BadgeNB info>
                                <TextNB>2</TextNB>
                            </BadgeNB>

                            <Text style={styles.subheader}>UI kitten</Text>
                            <Text>not implemented</Text>

                            <Text style={styles.subheader}>React Native Material UI</Text>
                            <BadgeRNMUI text="3" >
                                <IconRNMUI name="star" />
                            </BadgeRNMUI>
                        </Fragment>
                        <Fragment>
                            <SubheaderRNMUI style={stylesRNMUI.text} text="Button" />
                            <Text style={styles.subheader}>React Native</Text>
                            <Button
                                onPress={()=>{}}
                                title="Click Me!"
                            />

                            <Text style={styles.subheader}>React Native Elements</Text>
                            <ButtonRNE
                                title="Click Me!"
                            />

                            <Text style={styles.subheader}>Native Base</Text>
                            <ButtonNB>
                                <TextNB>Click Me!</TextNB>
                            </ButtonNB>
                            
                            <Text style={styles.subheader}>UI kitten</Text>
                            <RkButton>Click Me!</RkButton>

                            <Text style={styles.subheader}>React Native Material UI</Text>
                            <ButtonRNMUI raised primary text="Click Me!" />
                        </Fragment>
                        <Fragment>
                            <SubheaderRNMUI style={stylesRNMUI.text} text="Calendar" />
                            <Text style={styles.subheader}>React Native Elements</Text>
                            <Text>not implemented</Text>
                            
                            <Text style={styles.subheader}>Native Base</Text>
                            <DatePickerNB
                                defaultDate={new Date(2018, 4, 4)}
                                minimumDate={new Date(2018, 1, 1)}
                                maximumDate={new Date(2018, 12, 31)}
                                locale={"en"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Select date"
                                textStyle={{ color: "green" }}
                                placeHolderTextStyle={{ color: "#d3d3d3" }}
                                onDateChange={this.setDate}
                                disabled={false}
                            />
                            
                            <Text style={styles.subheader}>UI kitten</Text>
                            <Text>not implemented</Text>

                            <Text style={styles.subheader}>React Native Material UI</Text>
                            <Text>not implemented</Text>
                        </Fragment>
                        <Fragment>
                            <SubheaderRNMUI style={stylesRNMUI.text} text="Card" />
                            <Text style={styles.subheader}>React Native Elements</Text>
                            <CardRNE
                                title='React Native Elements'
                            >
                                <Text style={{marginBottom: 10}}>
                                    The idea with React Native Elements is more about component structure than actual design.
                                </Text>
                            </CardRNE>

                            <Text style={styles.subheader}>Native Base</Text>
                            <CardNB style={{width: '92%'}}>
                                <CardItemNB header bordered>
                                    <TextNB>NativeBase</TextNB>
                                </CardItemNB>
                                <CardItemNB bordered>
                                    <BodyNB>
                                        <TextNB>
                                            NativeBase is a free and open source framework that enable developers
                                        </TextNB>
                                    </BodyNB>
                                </CardItemNB>
                                <CardItemNB footer bordered>
                                    <TextNB>GeekyAnts</TextNB>
                                </CardItemNB>
                            </CardNB>
                            <Text style={styles.subheader}>UI kitten</Text>
                            <RkCard style={{width: '92%'}}>
                                <View rkCardHeader>
                                    <RkText rkType='header'>UI kitten</RkText>
                                </View>
                                <View rkCardContent>
                                    <RkText style={{textAlign:'center'}}>
                                        One morning, when Gregor Samsa woke from happy dreams, he found himself transformed in ...
                                    </RkText>
                                </View>
                                <View rkCardFooter>
                                    <RkButton rkType='small outline'>Learn More</RkButton>
                                    <RkButton rkType='small'>Read later</RkButton>
                                </View>
                            </RkCard>

                            <Text style={styles.subheader}>React Native Material UI</Text>
                            <CardRNMUI style={{width: '92%'}}>
                                <Text>Hello world!</Text>
                            </CardRNMUI>
                        </Fragment>
                        <Fragment>
                            <SubheaderRNMUI style={stylesRNMUI.text} text="CheckBox" />
                            <Text style={styles.subheader}>React Native</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <CheckBox value={true}/>
                                <Text style={{marginTop: 5}}>Click Here</Text>
                            </View>

                            <Text style={styles.subheader}>React Native Elements</Text>
                            <CheckBoxRNE
                                title='Click Here'
                                checked={true}
                            />
                            
                            <Text style={styles.subheader}>Native Base</Text>
                            <ListItemNB>
                                <CheckBoxNB checked={true} />
                                <TextNB>Click Here</TextNB>
                            </ListItemNB>

                            <Text style={styles.subheader}>UI kitten</Text>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <RkChoice rkType='redCheckMark' selected/>
                                <Text>Click Here</Text>
                            </View>
                            <Text style={styles.subheader}>React Native Material UI</Text>
                            <CheckboxRNMUI label="Click Here" value="agree" onCheck={()=>{}} checked={true} />
                        </Fragment>
                        <Fragment>
                            <SubheaderRNMUI style={stylesRNMUI.text} text="Input" />
                            <Text style={styles.subheader}>React Native</Text>
                            <TextInput
                                placeholder='placeholder'
                            />
                            
                            <Text style={styles.subheader}>React Native Elements</Text>
                            <InputRNE
                                label={'Label'}
                                placeholder='placeholder'
                            />

                            <Text style={styles.subheader}>Native Base</Text>
                            <InputNB
                                placeholder='placeholder'
                            />
                            
                            <Text style={styles.subheader}>UI kitten</Text>
                            <RkTextInput
                                label={'Label'}
                                placeholder='placeholder'
                            />

                            <Text style={styles.subheader}>React Native Material UI</Text>
                            <Text>not implemented</Text>
                        </Fragment>
                        <Fragment>
                            <SubheaderRNMUI style={stylesRNMUI.text} text="Icon" />
                            <Text style={styles.subheader}>React Native Elements</Text>
                            <IconRNE
                                name='sc-telegram'
                                type='evilicon'
                            />

                            <Text style={styles.subheader}>Native Base</Text>
                            <IconNB
                                name='sc-telegram'
                                type='EvilIcons'
                            />
                            
                            <Text style={styles.subheader}>UI kitten</Text>
                            <Text>not implemented</Text>

                            <Text style={styles.subheader}>React Native Material UI</Text>
                            <IconRNMUI
                                name="sc-telegram"
                                iconSet='EvilIcons'
                            />
                        </Fragment>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const stylesRNMUI = {
    text: {
        text: {
            fontSize: 25,
            color: 'red',
            textDecorationLine: 'underline',
            fontWeight: 'bold',
            marginTop: 30,
        },
    },
};

const styles = StyleSheet.create({
    subheader: {
        fontSize: 17,
        color: 'green',
        fontWeight: 'bold',
    },
});


export default UIKits;