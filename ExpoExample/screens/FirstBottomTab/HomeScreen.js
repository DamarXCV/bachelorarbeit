import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';

import WelcomeClass from './Welcome/WelcomeClass';
import WelcomeFunction from './Welcome/WelcomeFunction';
import Clock from './Clock';

class HomeScreen extends React.Component {
    handlePressAPIRequest = () => this.props.navigation.navigate('APIRequestScreen');
    handlePressTranslation = () => this.props.navigation.navigate('TranslationScreen');
    handlePressStateManagement = () => this.props.navigation.navigate('StateManagementScreen');
    handlePressForms = () => this.props.navigation.navigate('FormsScreen');
    handlePressVectorIcons = () => this.props.navigation.navigate('VectorIconsScreen');
    handlePressUIKits = () => this.props.navigation.navigate('UIKitsScreen');

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>Home Screen</Text>
                <WelcomeClass content="Class"/>
                <WelcomeFunction content="Function"/>
                <Clock/>
                <Button
                    buttonStyle={styles.button}
                    title="Go to API Request"
                    onPress={this.handlePressAPIRequest}
                />
                <Button
                    buttonStyle={styles.button}
                    title="Go to Translation"
                    onPress={this.handlePressTranslation}
                />
                <Button
                    buttonStyle={styles.button}
                    title="Go to State Management"
                    onPress={this.handlePressStateManagement}
                />
                <Button
                    buttonStyle={styles.button}
                    title="Go to Forms"
                    onPress={this.handlePressForms}
                />
                <Button
                    buttonStyle={styles.button}
                    title="Go to VectorIcons"
                    onPress={this.handlePressVectorIcons}
                />
                <Button
                    buttonStyle={styles.button}
                    title="Go to UI Kits"
                    onPress={this.handlePressUIKits}
                />
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
    button: {
        width: 300,
        margin: 10,
    },
});

export default HomeScreen;