import React from "react";
import { View, Text } from "react-native";

export default class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: new Date().toLocaleTimeString(),
        };
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }
    
    componentWillUnmount() {
        clearInterval(this.timerID);
    }
    
    tick() {
        this.setState({
            time: new Date().toLocaleTimeString(),
        });
    }

    render() {
        return (
            <View>
                <Text>{this.state.time}</Text>
            </View>
    )}
}
