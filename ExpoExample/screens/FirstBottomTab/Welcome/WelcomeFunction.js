import React from "react";
import { View, Text } from "react-native";
import PropTypes from 'prop-types';

export default function Welcome(props) {
    return (
        <View>
            <Text>Welcome: {props.content}</Text>
        </View>
)}

Welcome.propTypes = {
    content: PropTypes.string.isRequired,
}