import React, { PureComponent } from 'react';
import {
    string,
    func,
    bool,
} from 'prop-types';
import {
    View,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import {
    Input,
} from 'react-native-elements';
import { Entypo } from '@expo/vector-icons';

class TextInputWithHead extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            hidePassword: props.isPassword,
        };
    }

    handleChange = (value) => {
        const { onChange, name } = this.props;
        onChange(name, value);
    };

    handleTouch = () => {
        const { onTouch, name } = this.props;
        onTouch(name);
    };

    handlePasswordVisibility = () => {
        this.setState((prevState) => { // eslint-disable-line arrow-body-style
            return {
                hidePassword: !prevState.hidePassword,
            };
        });
    }

    handleRef = (input) => {
        const { setInputRef, name } = this.props;
        if (typeof setInputRef === 'function') {
            setInputRef(input, name);
        }
    }

    handleSubmitEditing = () => {
        const { onSubmitEditing, onSubmitEditingNext } = this.props;
        if (typeof onSubmitEditing === 'function' && onSubmitEditingNext.length > 0) {
            onSubmitEditing(onSubmitEditingNext);
        }
    }

    render() {
        const { hidePassword } = this.state;
        const {
            header,
            isPassword,
            error,
            ...attributes
        } = this.props;

        const multilineStyle = {};
        if (attributes.numberOfLines > 1) {
            multilineStyle.textAlignVertical = 'top';
        }

        return (
            <View style={styles.container}>
                <View key="passwordVisibility position view">
                    <Input
                        labelStyle={styles.label}
                        label={header}
                        {...attributes}
                        onChangeText={this.handleChange}
                        onBlur={this.handleTouch}
                        containerStyle={styles.inputContainer}
                        inputStyle={[styles.input, multilineStyle]}
                        underlineColorAndroid={0}
                        secureTextEntry={hidePassword}
                        textInputRef={this.handleRef}
                        onSubmitEditing={this.handleSubmitEditing}
                        errorStyle={styles.error}
                        errorMessage={ error ? error : ''}
                    />
                    {isPassword && (
                        <TouchableOpacity activeOpacity={0.8} style={styles.passwordVisibility} onPress={this.handlePasswordVisibility}>
                            <Entypo name={(hidePassword) ? 'eye-with-line' : 'eye'} color="white" style={styles.btnImage} />
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 10,
        width: '95%',
        alignSelf: 'center',
    },
    label: {
        width: '100%',
        marginLeft: 0,
        marginRight: 0,
        marginTop: 0,
        marginBottom: 0,
        color: 'black',
        textAlignVertical: 'top',
        fontSize: 14,
    },
    inputContainer: {
        width: '100%',
        marginLeft: 0,
        marginRight: 0,
    },
    input: {
        width: '100%',
        minHeight: 46,
        fontSize: 16,
        marginLeft: 0,
        marginRight: 0,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        paddingTop: 4,
        paddingBottom: 4,
        color: 'black',
        textAlignVertical: 'center',
    },
    passwordVisibility:
    {
        position: 'absolute',
        height: '100%',
        width: 46,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
    },
    error: {
        color: 'red',
        marginLeft: 0,
        marginRight: 0,
        marginTop: 0,
        marginBottom: 0,
    },
});

TextInputWithHead.propTypes = {
    header: string.isRequired,
    name: string.isRequired,
    onChange: func.isRequired,
    onTouch: func.isRequired,
    isPassword: bool,
    setInputRef: func,
    onSubmitEditing: func,
    onSubmitEditingNext: string,
};

TextInputWithHead.defaultProps = {
    isPassword: false,
    error: false,
    setInputRef: null,
    onSubmitEditing: null,
    onSubmitEditingNext: '',
};

export default TextInputWithHead;