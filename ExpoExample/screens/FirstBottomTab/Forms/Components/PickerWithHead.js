import React from 'react';
import {
    string,
    func,
    arrayOf,
    shape,
    oneOfType,
    number,
} from 'prop-types';
import {
    StyleSheet,
    View,
    Picker,
    Text,
    PixelRatio, 
    Dimensions,
    Platform,
} from 'react-native';

const pixelRatio = PixelRatio.get();
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const normalize = size => {
    if (pixelRatio >= 2 && pixelRatio < 3) {
        // iphone 5s and older Androids
        if (deviceWidth < 360) {
            return size * 0.95;
        }
        // iphone 5
        if (deviceHeight < 667) {
            return size;
            // iphone 6-6s
        } else if (deviceHeight >= 667 && deviceHeight <= 735) {
            return size * 1.15;
        }
        // older phablets
        return size * 1.25;
    } else if (pixelRatio >= 3 && pixelRatio < 3.5) {
        // catch Android font scaling on small machines
        // where pixel ratio / font scale ratio => 3:3
        if (deviceWidth <= 360) {
            return size;
        }
        // Catch other weird android width sizings
        if (deviceHeight < 667) {
            return size * 1.15;
            // catch in-between size Androids and scale font up
            // a tad but not too much
        }
        if (deviceHeight >= 667 && deviceHeight <= 735) {
            return size * 1.2;
        }
        // catch larger devices
        // ie iphone 6s plus / 7 plus / mi note 等等
        return size * 1.27;
    } else if (pixelRatio >= 3.5) {
        // catch Android font scaling on small machines
        // where pixel ratio / font scale ratio => 3:3
        if (deviceWidth <= 360) {
            return size;
            // Catch other smaller android height sizings
        }
        if (deviceHeight < 667) {
            return size * 1.2;
            // catch in-between size Androids and scale font up
            // a tad but not too much
        }
        if (deviceHeight >= 667 && deviceHeight <= 735) {
            return size * 1.25;
        }
        // catch larger phablet devices
        return size * 1.4;
    } else
      // if older device ie pixelRatio !== 2 || 3 || 3.5
    return size;
};
  

class PickerWithHead extends React.PureComponent {
    handleChange = (value) => {
        const { onChange, name } = this.props;
        onChange(name, value);
    };

    render() {
        const {
            header,
            items,
            value,
        } = this.props;

        let renderedItems;
        if (Array.isArray(items)) {
            renderedItems = items.map(element => <Picker.Item key={element.name + element.id} label={element.name} value={element.id} />);
        }
        else {
            renderedItems = Object.keys(items).map(key => <Picker.Item key={items[key].name + items[key].id} label={items[key].name} value={items[key].id} />);
        }


        return (
            <View style={styles.container}>
                <Text style={styles.label}>
                    {header}
                </Text>
                <Picker
                    keyboardShouldPersistTaps="always"
                    selectedValue={value}
                    style={styles.picker}
                    onValueChange={this.handleChange}
                    textStyle={styles.pickerText}
                >
                    {renderedItems}
                </Picker>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 10,
        width: '95%',
        alignSelf: 'center',
        marginLeft: 18,
    },
    label: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 15,
        marginBottom: 1,
        color: '#86939e',
        fontSize: normalize(12),
        ...Platform.select({
            ios: {
                fontWeight: 'bold',
            },
            android: {
                fontFamily: 'sans-serif',
                fontWeight: 'bold',
            },
        }),
        width: '100%',
        marginLeft: 0,
        marginRight: 0,
        marginTop: 0,
        marginBottom: 0,
        color: 'black',
        textAlignVertical: 'top',
        fontSize: 14,
    },
    picker: {
        width: '100%',
        minHeight: 46,
        marginLeft: 0,
        marginRight: 0,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        paddingTop: 4,
        paddingBottom: 4,
        color: 'black',
    },
    pickerText: {
        fontSize: 16,
    },
});

PickerWithHead.propTypes = {
    header: string.isRequired,
    name: string.isRequired,
    items: arrayOf(shape({
        id: oneOfType([
            number,
            string,
        ]).isRequired,
        name: string.isRequired,
    }).isRequired).isRequired,
    value: oneOfType([
        number,
        string,
    ]).isRequired,
    onChange: func.isRequired,
};

export default PickerWithHead;
