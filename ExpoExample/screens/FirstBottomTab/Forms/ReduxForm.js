import React from "react";
import { Button, View, Text, TextInput } from "react-native";

import { Field, reduxForm } from 'redux-form'

import { Provider } from 'react-redux';
import store from "./ReduxComponents/store";

const submit = values => {
    console.log('submitting form', values)
}
  
const renderInput = ({ input: { onChange, ...restInput }}) => {
    return <TextInput  onChangeText={onChange} {...restInput} />
}

const Form = props => {
    const { handleSubmit } = props
  
    return (
        <View >
            <Text>Email:</Text>
            <Field name="email" component={renderInput} />
            <Button title="submit" onPress={handleSubmit(submit)}/>
        </View>
    )
}

const MyReduxForm = reduxForm({
    form: 'form'
})(Form)

class ReduxFormModal extends React.Component {

    render() {
        return (
            <Provider store={store}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>ReduxForm Screen</Text>
                    <MyReduxForm/>
                </View>
            </Provider>
        );
    }
}


export default ReduxFormModal; 
