import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';

import { withFormik } from 'formik';
import * as Yup from 'yup';

import InputWithHead from './Components/TextInputWithHead'
import PickerWithHead from './Components/PickerWithHead'

const Gender = [
    {
        id: 'f',
        name: 'female',
    },
    {
        id: 'm',
        name: 'male',
    },
    {
        id: 'o',
        name: 'other',
    },
];

const FormHOC = withFormik({
    mapPropsToValues: () => ({
        string: '',
        number: '0',
        gender: 'm',
    }),
  
    validationSchema: Yup.object().shape({
        string: Yup.string()
            .required(),

        number: Yup.number()
            .min(6)
            .required(),
    }),
  
    handleSubmit: (values, { setSubmitting }) => {
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },
  
    displayName: 'Form',
});



class FormikHOC extends React.Component {
    render() {
        const {
            setFieldValue,
            setFieldTouched,
            values,
            touched,
            errors,
            handleSubmit,
        } = this.props;

        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>FormikHOC Screen</Text>
                <InputWithHead
                    name="string"
                    header="String"
                    placeholder="Your String here..."
                    value={values.string}
                    error={touched.string && errors.string}
                    onChange={setFieldValue}
                    onTouch={setFieldTouched}
                    autoCorrect={false}
                />
                <InputWithHead
                    name="number"
                    header="Number"
                    placeholder="Your String here..."
                    value={values.number}
                    error={touched.number && errors.number}
                    onChange={setFieldValue}
                    onTouch={setFieldTouched}
                    autoCorrect={false}
                    keyboardType="numeric"
                />
                <PickerWithHead
                    name="gender"
                    header="Gender"
                    items={Gender}
                    value={values.gender}
                    onChange={setFieldValue}
                />
                <Button
                    buttonStyle={styles.button}
                    onPress={handleSubmit}
                    title="Submit"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
    button: {
        width: 300,
        margin: 10,
    },
});

export default FormHOC(FormikHOC);