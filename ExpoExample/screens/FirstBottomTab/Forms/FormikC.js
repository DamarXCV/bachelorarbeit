import React, { Fragment } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';

import { Formik } from 'formik';
import * as Yup from 'yup';

import InputWithHead from './Components/TextInputWithHead'
import PickerWithHead from './Components/PickerWithHead'

const Gender = [
    {
        id: 'f',
        name: 'female',
    },
    {
        id: 'm',
        name: 'male',
    },
    {
        id: 'o',
        name: 'other',
    },
];

const validationSchema = Yup.object().shape({
    string: Yup.string()
        .required(),

    number: Yup.number()
        .min(6)
        .required(),
});

class FormikC extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>FormikC Screen</Text>
                <Formik
                    initialValues = {{
                        string: '',
                        number: '0',
                        gender: 'm',
                    }}
                    onSubmit = {(values, { setSubmitting }) => {
                        setTimeout(() => {
                            alert(JSON.stringify(values, null, 2));
                            setSubmitting(false);
                        }, 1000);
                    }}
                    validationSchema = {validationSchema}
                >
                    {props => (
                    <Fragment>
                        <InputWithHead
                            name="string"
                            header="String"
                            placeholder="Your String here..."
                            value={props.values.string}
                            error={props.touched.string && props.errors.string}
                            onChange={props.setFieldValue}
                            onTouch={props.setFieldTouched}
                            autoCorrect={false}
                        />
                        <InputWithHead
                            name="number"
                            header="Number"
                            placeholder="Your String here..."
                            value={props.values.number}
                            error={props.touched.number && props.errors.number}
                            onChange={props.setFieldValue}
                            onTouch={props.setFieldTouched}
                            autoCorrect={false}
                            keyboardType="numeric"
                        />
                        <PickerWithHead
                            name="gender"
                            header="Gender"
                            items={Gender}
                            value={props.values.gender}
                            onChange={props.setFieldValue}
                        />
                        <Button
                            buttonStyle={styles.button}
                            onPress={props.handleSubmit}
                            title="Submit"
                        />
                    </Fragment>
                    )}
                </Formik>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
    button: {
        width: 300,
        margin: 10,
    },
});

export default FormikC;