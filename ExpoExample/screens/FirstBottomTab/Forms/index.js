import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';

class Forms extends React.Component {
    handlePressTcombFormNative = () => this.props.navigation.push('TcombFormNativeScreen');
    handlePressFormikHOC = () => this.props.navigation.push('FormikHOCScreen');
    handlePressFormikC = () => this.props.navigation.push('FormikCScreen');

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>Forms Screen</Text>
                <Button
                    buttonStyle={styles.button}
                    title="Go to TcombFormNative"
                    onPress={this.handlePressTcombFormNative}
                />
                <Button
                    buttonStyle={styles.button}
                    title="Go to FormikHOC"
                    onPress={this.handlePressFormikHOC}
                />
                <Button
                    buttonStyle={styles.button}
                    title="Go to Formik Component"
                    onPress={this.handlePressFormikC}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
    button: {
        width: 300,
        margin: 10,
    },
});

export default Forms;