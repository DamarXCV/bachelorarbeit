import React from "react";
import { ScrollView, Text, Button } from "react-native";
import tfn from 'tcomb-form-native';

class TcombFormNative extends React.Component {
    Gender = tfn.enums({
        0: 'Male',
        1: 'Female',
        2: 'Apache',
        3: 'Toastbrot',
        4: 'Dinkelbrot'
    }, 'gender');

    Positive = tfn.refinement(tfn.Number, function (n) {
        return n >= 0;
    });
    
    Documents = tfn.struct({
        type: tfn.Number,
        value: tfn.String
    })

    AccountType = tfn.enums.of([
        'type 1',
        'type 2',
        'other'
    ], 'AccountType')

    Person = tfn.struct({
        string: tfn.String,
        optionalString: tfn.maybe(tfn.String),
        number: tfn.Number,
        positiveNumber: this.Positive,
        rememberMe: tfn.Boolean,
        date: tfn.Date,
        time: tfn.Date,
        picker: this.Gender,
        list: tfn.list(this.Documents),
    });

    Options = {
        fields: {
            name: {
                error: "insert a valid name"
            },
            password: {
                password: true,
                secureTextEntry: true
            },
            date: {
                mode: "date",
                config: {
                    format: (date) => (date.getDay() + "." + date.getMonth() + "." + date.getFullYear()),
                }
            },
            time: {
                mode: "time",
                config: {
                    format: (date) => (date.getHours() + ":" + date.getMinutes()),
                }
            },
        }
    }
    
    Values = {
        date: new Date(),
        time: new Date(),
        gender: 1,
    }

    onPressSubmit = () => {
        var value = this.refs.form.getValue();
        
        if (value) {
            alert(value.name);
        }
    }
    
    render() {
        return (
            <ScrollView>
                <Text>TcombFormNative Screen</Text>
                <tfn.form.Form
                    ref="form"
                    type={this.Person}
                    value={this.Values}
                    options={this.Options}
                />
                <Button
                    title="Submit"
                    onPress={this.onPressSubmit}
                />
            </ScrollView>
          );
    }
}

export default TcombFormNative; 
