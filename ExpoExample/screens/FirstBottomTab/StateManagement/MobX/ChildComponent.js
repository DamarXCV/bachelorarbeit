import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';

import { observer, inject } from 'mobx-react';

@inject('ApplicationState')
@observer
class ChildComponent extends React.Component {
    render() {
        return (
            <View style={{ alignItems: 'center' }}>
                <Text>Counter: {this.props.ApplicationState.counter}</Text>
                <Button
                    buttonStyle={styles.button}
                    title="Increase Counter"
                    onPress={this.props.ApplicationState.increaseCounter}
                />
                <Text>Persist Counter: {this.props.ApplicationState.persistCounter}</Text>
                <Button
                    buttonStyle={styles.button}
                    title="Increase Persist Counter"
                    onPress={this.props.ApplicationState.increasePersistCounter}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        width: 300,
        margin: 10,
    },
});

export default ChildComponent;
