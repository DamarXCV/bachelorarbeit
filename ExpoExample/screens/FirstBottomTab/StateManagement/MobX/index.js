import React from "react";
import { View, Text, StyleSheet } from "react-native";

import { Provider, observer } from "mobx-react";
import ApplicationState from "./MobXComponents/store";

import ChildComponent from "./ChildComponent";

@observer
class MobX extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>MobX Screen</Text>

                <Provider ApplicationState={ApplicationState}>
                    <ChildComponent/>
                </Provider>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
});

export default MobX;