import { AsyncStorage } from 'react-native';
import { observable, action, computed } from 'mobx';
import { create, persist } from 'mobx-persist';

class ApplicationState {
    constructor() {};

    @observable _Counter = 0;
    @persist @observable _PersistCounter = 0;

    @computed get counter() { return this._Counter };
    @computed get persistCounter() { return this._PersistCounter };

    @action increaseCounter = () => {
        this._Counter++;
    };
    @action increasePersistCounter = () => {
        this._PersistCounter++;
    };
};

const state = new ApplicationState();

const hydrate = create({
  storage: AsyncStorage,
  jsonify: true,
})

hydrate('persistedState', state);

export default state;
