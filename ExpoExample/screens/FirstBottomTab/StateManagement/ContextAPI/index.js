import React from "react";
import { View, Text, StyleSheet } from "react-native";

import { AppProvider } from "./ContextAPIComponents/store";

import ChildComponent from "./ChildComponent";

class ContextAPI extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>ContextAPI Screen</Text>
                
                <AppProvider>
                    <ChildComponent/>
                </AppProvider>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
});

export default ContextAPI;