import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';

import { AppContext } from "./ContextAPIComponents/store";

class ChildComponent extends React.Component {
    render() {
        return (
            <View>
                <AppContext.Consumer>
                    {(context) => (
                        <View style={{ alignItems: 'center' }}>
                            <Text>Counter: {context.counter}</Text>
                            <Button
                                buttonStyle={styles.button}
                                title="Increase Counter"
                                onPress={context.increaseCounter}
                            />
                            <Text>Persist Counter: {context.persistData.counter}</Text>
                            <Button
                                buttonStyle={styles.button}
                                title="Increase Persist Counter"
                                onPress={context.increasePersistCounter}
                            />
                        </View>
                    )}
                </AppContext.Consumer>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        width: 300,
        margin: 10,
    },
});

export default ChildComponent;
