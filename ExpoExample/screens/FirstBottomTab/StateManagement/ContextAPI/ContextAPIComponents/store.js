import React from "react";
import { AsyncStorage } from 'react-native';

const AppContext = React.createContext()

class AppProvider extends React.Component {
    saveStore = () => {
        if(this.state.persistData){
            AsyncStorage.setItem('storage', JSON.stringify(this.state.persistData));
        }
    };

    getStore = () => {
        AsyncStorage.getItem('storage').then(value => {
            if (value != null) {
                this.setState((oldState, props) => {
                    return { 
                        ...oldState,
                        persistData: JSON.parse(value),
                    };
                });
            }
        });
    };
    
    componentDidMount() {
        this.getStore();
    }

    state = {
        counter: 0,
        persistData: {
            counter: 0,
        },

        increaseCounter: () => {
            this.setState((oldState, props) => {
                return { 
                    ...oldState,
                    counter: oldState.counter + 1,
                };
            });
        },
        increasePersistCounter: () => {
            this.setState((oldState, props) => {
                return { 
                    ...oldState,
                    persistData: {
                        ...oldState.persistData,
                        counter: oldState.persistData.counter + 1,
                    },
                };
            });
            this.saveStore();
        }
    }

    render() {
        return (
            <AppContext.Provider value={this.state}>
                {this.props.children}
            </AppContext.Provider>
        )
    }
}

export { AppProvider, AppContext };