import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';

import { connect } from 'react-redux'
import { increaseCounter, increasePersistCounter } from './ReduxComponents/actions'

class ChildComponent extends React.Component {
    render() {
        return (
            <View style={{ alignItems: 'center' }}>
                <Text>Counter: {this.props.counter}</Text>
                <Button
                    buttonStyle={styles.button}
                    title="Increase Counter"
                    onPress={this.props.increaseCounter}
                />
                <Text>Persist Counter: {this.props.persistCounter}</Text>
                <Button
                    buttonStyle={styles.button}
                    title="Increase Persist Counter"
                    onPress={this.props.increasePersistCounter}
                />
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    button: {
        width: 300,
        margin: 10,
    },
});

const mapStateToProps = (state, ownProps) => {
    return {
        counter: state.main.counter,
        persistCounter: state.main.persistCounter
    }
}

const mapDispatchToProps = {
    increaseCounter,
    increasePersistCounter,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChildComponent)
