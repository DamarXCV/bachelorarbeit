import React from "react";
import { View, Text, StyleSheet } from "react-native";

import { Provider } from 'react-redux';
import store from "./ReduxComponents/store";

import ChildComponent from "./ChildComponent";

class ReduxScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>Redux Screen</Text>
                
                <Provider store={store}>
                    <ChildComponent/>
                </Provider>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
});

export default ReduxScreen;