import { combineReducers } from 'redux';
import { AsyncStorage } from 'react-native';
import { persistReducer } from 'redux-persist';

const INITIAL_STATE = {
    counter: 0,
    persistCounter: 0,
};

const mainReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'INCREASE_COUNTER':
            return {
                ...state,
                counter: state.counter + 1,
            };
        case 'INCREASE_PERSIST_COUNTER':
            return {
                ...state,
                persistCounter: state.persistCounter + 1,
            };

        default:
            return state
    }
};

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: [
        'counter',
    ],
}
  
const persistedReducer = persistReducer(persistConfig, mainReducer)

export default combineReducers({
    main: persistedReducer,
});
