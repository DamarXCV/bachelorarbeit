import { createStore, applyMiddleware } from "redux";
import { persistStore } from 'redux-persist';

// import thunk from 'redux-thunk';
// import logger from 'redux-logger';

import rootReducer from "./reducers";

// let middleware = [thunk];

// if (__DEV__) {
//     middleware = [...middleware, logger];
// }
// else {
//     middleware = [...middleware];
// }


const store = createStore(
    rootReducer,
    // rootReducer,
    {},
    // applyMiddleware(...middleware),
);

let persistor = persistStore(store);

export default store;