
export const increaseCounter = () => ({
    type: 'INCREASE_COUNTER',
})

export const increasePersistCounter = () => ({
    type: 'INCREASE_PERSIST_COUNTER',
})