import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';

class StateManagement extends React.Component {
    handlePressRedux = () => this.props.navigation.push('ReduxScreen');
    handlePressMobX = () => this.props.navigation.push('MobXScreen');
    handlePressContextAPI = () => this.props.navigation.push('ContextAPIScreen');
    
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>StateManagement Screen</Text>
                <Button
                    buttonStyle={styles.button}
                    title="Go to Redux"
                    onPress={this.handlePressRedux}
                />
                <Button
                    buttonStyle={styles.button}
                    title="Go to MobX"
                    onPress={this.handlePressMobX}
                />
                <Button
                    buttonStyle={styles.button}
                    title="Go to ContextAPI"
                    onPress={this.handlePressContextAPI}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
    button: {
        width: 300,
        margin: 10,
    },
});

export default StateManagement;