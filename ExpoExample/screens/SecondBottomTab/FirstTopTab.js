import React from "react";
import { View, Text, StyleSheet } from "react-native";

class FirstTopTab extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>FirstTopTab Screen</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
});

export default FirstTopTab;