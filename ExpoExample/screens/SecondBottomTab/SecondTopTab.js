import React from "react";
import { View, Text, StyleSheet } from "react-native";

class SecondTopTab extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>SecondTopTab Screen</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
});

export default SecondTopTab;