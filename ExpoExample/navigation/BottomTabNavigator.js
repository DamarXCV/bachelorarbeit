import React from 'react';
import { Platform } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';

import TopTabNavigator from './TopTabNavigator'

import ModalsScreen from './../screens/ThirdBottomTab/ModalsScreen'
import HomeScreen from './../screens/FirstBottomTab/HomeScreen'

import APIRequestScreen from './../screens/FirstBottomTab/APIRequest'
import FetchAPIScreen from './../screens/FirstBottomTab/APIRequest/FetchAPI'
import AxiosScreen from './../screens/FirstBottomTab/APIRequest/Axios'

import TranslationScreen from './../screens/FirstBottomTab/Translation'

import StateManagementScreen from './../screens/FirstBottomTab/StateManagement'
import ReduxScreen from './../screens/FirstBottomTab/StateManagement/Redux'
import MobXScreen from './../screens/FirstBottomTab/StateManagement/MobX'
import ContextAPIScreen from './../screens/FirstBottomTab/StateManagement/ContextAPI'

import FormsScreen from './../screens/FirstBottomTab/Forms'
import TcombFormNativeScreen from './../screens/FirstBottomTab/Forms/TcombFormNative'
import FormikHOCScreen from './../screens/FirstBottomTab/Forms/FormikHOC'
import FormikCScreen from './../screens/FirstBottomTab/Forms/FormikC'

import VectorIconsScreen from './../screens/FirstBottomTab/VectorIcons'

import UIKitsScreen from './../screens/FirstBottomTab/UIKits'

const MaterialHeaderButton = props => (
    <HeaderButton {...props} IconComponent={MaterialIcons} iconSize={23} color="black" />
);

const HeaderDrawerButton = navigation => (
    <HeaderButtons left HeaderButtonComponent={MaterialHeaderButton}>
        <Item
            title="drawer"
            iconName={'menu'}
            onPress={() => navigation.openDrawer()}
        />
    </HeaderButtons>
)

const FirstBottomTabStack = createStackNavigator({
    HomeScreen: {
        screen: HomeScreen,
        navigationOptions: {
            title: 'Home',
        }
    },

    APIRequestScreen: {
        screen: APIRequestScreen,
        navigationOptions: {
            title: 'API Request',
        }
    },
    FetchAPIScreen: {
        screen: FetchAPIScreen,
        navigationOptions: {
            title: 'FetchAPI',
        }
    },
    AxiosScreen: {
        screen: AxiosScreen,
        navigationOptions: {
            title: 'axios',
        }
    },

    TranslationScreen: {
        screen: TranslationScreen,
        navigationOptions: {
            title: 'Translation',
        }
    },

    StateManagementScreen: {
        screen: StateManagementScreen,
        navigationOptions: {
            title: 'State Management',
        }
    },
    ReduxScreen: {
        screen: ReduxScreen,
        navigationOptions: {
            title: 'Redux',
        }
    },
    MobXScreen: {
        screen: MobXScreen,
        navigationOptions: {
            title: 'MobX',
        }
    },
    ContextAPIScreen: {
        screen: ContextAPIScreen,
        navigationOptions: {
            title: 'ContextAPI',
        }
    },
    
    FormsScreen: {
        screen: FormsScreen,
        navigationOptions: {
            title: 'Forms',
        }
    },
    TcombFormNativeScreen: {
        screen: TcombFormNativeScreen,
        navigationOptions: {
            title: 'TcombFormNative',
        }
    },
    FormikHOCScreen: {
        screen: FormikHOCScreen,
        navigationOptions: {
            title: 'FormikHOC',
        }
    },
    FormikCScreen: {
        screen: FormikCScreen,
        navigationOptions: {
            title: 'FormikC',
        }
    },

    VectorIconsScreen: {
        screen: VectorIconsScreen,
        navigationOptions: {
            title: 'Vector Icons',
        }
    },

    UIKitsScreen: {
        screen: UIKitsScreen,
        navigationOptions: {
            title: 'UI Kits',
        }
    },
});

FirstBottomTabStack.navigationOptions = {
    tabBarIcon: ({focused, tintColor}) => (<Ionicons name={"md-home"} size={25} color={tintColor} />),
};

const SecondBottomTabStack = createStackNavigator({
    TopTabNavigator: {
        screen: TopTabNavigator,
        navigationOptions: ({ navigation }) => ({
            title: 'TopTabNavigator',
            headerLeft: HeaderDrawerButton(navigation),
        }),
    }
});

SecondBottomTabStack.navigationOptions = {
    tabBarIcon: ({focused, tintColor}) => (<Ionicons name={"md-star"} size={25} color={tintColor} />),
    headerMode: 'screen',
};

const ThirdBottomTabStack = createStackNavigator({
    ModalsScreen: {
        screen: ModalsScreen,
        navigationOptions: ({ navigation }) => ({
            title: 'ModalsScreen',
            headerMode: 'screen',
            headerLeft: HeaderDrawerButton(navigation),
        }),
    }
});

ThirdBottomTabStack.navigationOptions = {
    tabBarIcon: ({focused, tintColor}) => (<Ionicons name={"md-construct"} size={25} color={tintColor} />),
    headerMode: 'screen',
};

const RouteConfigs = {
    FirstBottomTab: FirstBottomTabStack,
    SecondBottomTab: SecondBottomTabStack,
    ThirdBottomTab: ThirdBottomTabStack,
};

const BottomTabNavigator = Platform.OS === 'ios' ?
    createBottomTabNavigator(RouteConfigs) :
    createMaterialBottomTabNavigator(RouteConfigs);

export default BottomTabNavigator;