import React from 'react';
import { createAppContainer, createStackNavigator, createDrawerNavigator } from 'react-navigation';

import StackScreen from '../screens/ModalStack/StackScreen'
import BottomTabNavigator from './BottomTabNavigator'

import FirstDrawerScreen from './../screens/Drawer/FirstDrawerScreen'
import SecondDrawerScreen from './../screens/Drawer/SecondDrawerScreen'

import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import { MaterialIcons } from '@expo/vector-icons';

const MaterialHeaderButton = props => (
    <HeaderButton {...props} IconComponent={MaterialIcons} iconSize={23} color="black" />
);

const HeaderBackButton = navigation => (
    <HeaderButtons left HeaderButtonComponent={MaterialHeaderButton}>
        <Item
            title="back"
            iconName={'arrow-back'}
            onPress={() => navigation.pop()}
        />
    </HeaderButtons>
)

const FirstDrawerItemStackNavigator = createStackNavigator({
    FirstDrawerScreen: {
        screen: FirstDrawerScreen,
        navigationOptions: ({ navigation }) => ({
            title: 'FirstDrawerScreen',
            headerLeft: HeaderBackButton(navigation),
        }),
    },
});

const SecondDrawerItemStackNavigator = createStackNavigator({
    SecondDrawerScreen: {
        screen: SecondDrawerScreen,
        navigationOptions: ({ navigation }) => ({
            title: 'SecondDrawerScreen',
            headerLeft: HeaderBackButton(navigation),
        }),
    },
});

const DrawerStackNavigator = createDrawerNavigator({
    Main: {
        screen: BottomTabNavigator,
        navigationOptions: {
            drawerLabel: () => null,
        }
    },
    FirstDrawerItem: FirstDrawerItemStackNavigator,
    SecondDrawerItem: SecondDrawerItemStackNavigator,
});

const ModalStackNavigator = createStackNavigator({
    StackScreen: {
        screen: StackScreen,
        navigationOptions: ({ navigation }) => ({
            title: 'ModalStack',
            headerLeft: HeaderBackButton(navigation),
        }),
    },
});

const RootStack = createStackNavigator({
    DrawerStack: {
        screen: DrawerStackNavigator,
    },
    Modals: {
        screen: ModalStackNavigator,
    },
},
{
    headerMode: 'none',
});

export default createAppContainer(RootStack);
