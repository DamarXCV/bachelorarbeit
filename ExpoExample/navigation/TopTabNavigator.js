import { createMaterialTopTabNavigator } from 'react-navigation';

import FirstTopTab from './../screens/SecondBottomTab/FirstTopTab'
import SecondTopTab from './../screens/SecondBottomTab/SecondTopTab'

export default createMaterialTopTabNavigator({
    FirstTopTab: {
        screen: FirstTopTab,
    },
    SecondTopTab: {
        screen: SecondTopTab,
    }
});