import React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import {
  Scene,
  Router,
  Tabs,
  Stack,
  Drawer,
} from 'react-native-router-flux';
import HomeScreen from './../screens/FirstBottomTab/HomeScreen';
import ModalsScreen from '../screens/SecondBottomTab/ModalsScreen';
import StackScreen from './../screens/ModalStack/StackScreen';
import DrawerContent from './../components/DrawerContent';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabBarStyle: {
    backgroundColor: '#eee',
  },
  tabBarSelectedItemStyle: {
    backgroundColor: '#ddd',
  },
});

const AppNavigator = () => (
    <Router>
        <Stack>
            <Drawer
                hideNavBar
                key="drawer"
                contentComponent={DrawerContent}
                drawerWidth={300}
            >
                <Tabs
                    key="tabbar"
                    backToInitial
                    swipeEnabled
                    tabBarStyle={styles.tabBarStyle}
                    activeBackgroundColor="white"
                    inactiveBackgroundColor="rgba(255, 0, 0, 0.5)"
                >
                    <Scene
                        key="first"
                        component={HomeScreen}
                        title="Home"
                    />
                    <Stack>
                        <Scene
                            key="second"
                            component={ModalsScreen}
                            title="Second"
                        />
                        <Scene
                            key="stack"
                            component={StackScreen}
                            title="Stack"
                        />
                    </Stack>
                </Tabs>
            </Drawer>
        </Stack>
    </Router>
);

export default AppNavigator;
