import React from "react";
import { View, Text, Button, StyleSheet } from "react-native";
import { Actions } from 'react-native-router-flux';

class ModalsScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>Modals Screen</Text>
                <Button
                    buttonStyle={styles.button}
                    title="Go to StackScreen"
                    onPress={Actions.stack}
                />
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
    button: {
        width: 300,
        margin: 10,
    },
});

export default ModalsScreen;