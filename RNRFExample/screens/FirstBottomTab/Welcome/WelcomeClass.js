import React from "react";
import { View, Text } from "react-native";
import PropTypes from 'prop-types';

export default class Welcome extends React.Component {
    render() {
        return (
            <View>
                <Text>Welcome: {this.props.content}</Text>
            </View>
    )}
}

Welcome.propTypes = {
    content: PropTypes.string.isRequired,
}