import React from "react";
import { View, Text, StyleSheet } from "react-native";

import WelcomeClass from './Welcome/WelcomeClass';
import WelcomeFunction from './Welcome/WelcomeFunction';
import Clock from './Clock';

class HomeScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>Home Screen</Text>
                <WelcomeClass content="Class"/>
                <WelcomeFunction content="Function"/>
                <Clock/>
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
    button: {
        width: 300,
        margin: 10,
    },
});

export default HomeScreen;