import { Navigation } from "react-native-navigation";
import screenRegistration from "./screens"
import Ionicons from 'react-native-vector-icons/Ionicons'

async function prepareIcons() {

    const icons = await Promise.all([
        Ionicons.getImageSource('md-home', 25),
        Ionicons.getImageSource('md-star', 25),
        Ionicons.getImageSource('md-construct', 25),
        Ionicons.getImageSource('md-menu', 25),
    ]);

    const [ firstBottomTabIcon, secondBottomTabIcon, thirdBottomTabIcon, burger ] = icons;
    return { firstBottomTabIcon, secondBottomTabIcon, thirdBottomTabIcon, burger };
}

screenRegistration();

async function startApp() {
    const icons = await prepareIcons();

    Navigation.setRoot({
        root: {
            sideMenu: {
                id: 'main',
                left: {
                    component: {
                        name: 'AppScreen',
                    },
                },
                center: {
                    bottomTabs: {
                        id: 'BottomTabs',
                        children: [{
                            stack: {
                                id: 'FirstBottomTab',
                                children: [{
                                    component: {
                                        name: 'App.FirstBottomTab.HomeScreen',
                                    }
                                }],
                                options: {
                                    topBar: {
                                        title: {
                                            text: 'Home',
                                        },
                                    },
                                    bottomTab: {
                                        text: 'FirstBottomTab',
                                        icon: icons.firstBottomTabIcon,
                                    }
                                }
                            }
                        },
                        {
                            stack: {
                                id: 'SecondBottomTab',
                                children: [{
                                    topTabs: {
                                        id: 'topTabs',
                                        children: [{
                                            component: {
                                                id: 'Home',
                                                name: 'App.SecondBottomTab.FirstTopTabScreen',
                                                options: {
                                                    topTab: {
                                                        title: 'FirstTopTab',
                                                    },
                                                },
                                            },
                                        },
                                        {
                                            component: {
                                                id: 'Home2',
                                                name: 'App.SecondBottomTab.SecondTopTabScreen',
                                                options: {
                                                    topTab: {
                                                        title: 'SecondTopTab',
                                                    },
                                                },
                                            },
                                        }],
                                    },
                                }],
                                options: {
                                    topBar: {
                                        title: {
                                            text: 'TopTabNavigator',
                                        },
                                        height: 100,
                                        leftButtons: [{
                                            id: 'sideMenu',
                                            icon: icons.burger,
                                        }]
                                    },
                                    bottomTab: {
                                        text: 'SecondBottomTab',
                                        icon: icons.secondBottomTabIcon,
                                    }
                                }
                            }
                        },
                        {
                            stack: {
                                id: 'ThirdBottomTab',
                                children: [{
                                    component: {
                                        id: 'ModalsScreen',
                                        name: 'App.ThirdBottomTab.ModalsScreen',
                                        options: {
                                            topBar: {
                                                leftButtons: [{
                                                    id: 'sideMenu',
                                                    icon: icons.burger,
                                                }]
                                            },
                                        }
                                    }
                                }],
                                options: {
                                    topBar: {
                                        title: {
                                            text: 'ModalsScreen',
                                        },
                                    },
                                    bottomTab: {
                                        text: 'ThirdBottomTab',
                                        icon: icons.thirdBottomTabIcon,
                                    }
                                }
                            }
                        }]
                    }
                }
            }
        }
    });
}

startApp();
