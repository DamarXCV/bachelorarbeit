import { Navigation } from "react-native-navigation";

import AppScreen from './../screens/App';
import HomeScreen from './../screens/FirstBottomTab/HomeScreen';
import TranslationScreen from './../screens/FirstBottomTab/Translation';
import VectorIcosScreen from './../screens/FirstBottomTab/VectorIcons';

import FirstTopTabScreen from './../screens/SecondBottomTab/FirstTopTab';
import SecondTopTabScreen from './../screens/SecondBottomTab/SecondTopTab';

import ModalsScreen from './../screens/ThirdBottomTab/ModalsScreen';
import StackScreen from './../screens/ModalStack/StackScreen';

export default () => {
    Navigation.registerComponent(`AppScreen`, () => AppScreen);
    
    Navigation.registerComponent(`App.FirstBottomTab.HomeScreen`, () => HomeScreen);
    Navigation.registerComponent(`App.FirstBottomTab.TranslationScreen`, () => TranslationScreen);
    Navigation.registerComponent(`App.FirstBottomTab.VectorIcosScreen`, () => VectorIcosScreen);

    Navigation.registerComponent(`App.SecondBottomTab.FirstTopTabScreen`, () => FirstTopTabScreen);
    Navigation.registerComponent(`App.SecondBottomTab.SecondTopTabScreen`, () => SecondTopTabScreen);

    Navigation.registerComponent(`App.ThirdBottomTab.ModalsScreen`, () => ModalsScreen);
    Navigation.registerComponent(`App.ModalStack.StackScreen`, () => StackScreen);
}