import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';
import { Navigation } from "react-native-navigation";

class ModalsScreen extends React.Component {
    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
    }
    
    navigationButtonPressed({ buttonId }) {
        if (buttonId === "sideMenu") {
            Navigation.mergeOptions('main', {
                sideMenu: {
                    left: {
                        visible: true,
                    }
                }
           });
        }
    }
    
    handlePress = () => Navigation.push(this.props.componentId, {
        component: {
            name: 'App.ModalStack.StackScreen',
            options: {
                topBar: {
                    title: {
                        text: 'StackScreen'
                    }
                }
            }
        }
    });

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>ModalsScreen Screen</Text>
                <Button
                    buttonStyle={styles.button}
                    title="Go to StackScreen"
                    onPress={this.handlePress}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
    button: {
        width: 300,
        margin: 10,
    },
});

export default ModalsScreen; 
