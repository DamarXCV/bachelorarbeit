import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Navigation } from "react-native-navigation";

class SecondTopTab extends React.Component {
    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
    }
    
    navigationButtonPressed({ buttonId }) {
        if (buttonId === "sideMenu") {
            Navigation.mergeOptions('main', {
                sideMenu: {
                    left: {
                        visible: true,
                    }
                }
           });
        }
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>SecondTopTab Screen</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
});

export default SecondTopTab;