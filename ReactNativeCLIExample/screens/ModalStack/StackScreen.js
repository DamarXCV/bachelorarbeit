import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Navigation } from "react-native-navigation";
import { Button } from 'react-native-elements';

class ModalsScreen extends React.Component {
    handlePress = () => Navigation.push(this.props.componentId, {
        component: {
            name: 'App.ModalStack.StackScreen',
            options: {
                topBar: {
                    title: {
                        text: 'ModalStack'
                    }
                }
            }
        }
    });

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>Modals Screen</Text>
                <Button
                    buttonStyle={styles.button}
                    title="Go to StackScreen"
                    onPress={this.handlePress}
                />
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
    button: {
        width: 300,
        margin: 10,
    },
});

export default ModalsScreen;