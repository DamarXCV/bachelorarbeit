import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-elements';
import { Navigation } from "react-native-navigation";

class HomeScreen extends React.Component {
    handlePressTranslation = () => Navigation.push(this.props.componentId, {
        component: {
            name: 'App.FirstBottomTab.TranslationScreen',
            options: {
                topBar: {
                    title: {
                        text: 'TranslationScreen'
                    }
                }
            }
        }
    });
    handlePressVectorIcons = () => Navigation.push(this.props.componentId, {
        component: {
            name: 'App.FirstBottomTab.VectorIcosScreen',
            options: {
                topBar: {
                    title: {
                        text: 'VectorIcosScreen'
                    }
                }
            }
        }
    });

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>Home Screen</Text>
                <Button
                    buttonStyle={styles.button}
                    title="Go to Translation"
                    onPress={this.handlePressTranslation}
                />
                <Button
                    buttonStyle={styles.button}
                    title="Go to VectorIcons"
                    onPress={this.handlePressVectorIcons}
                />
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
    button: {
        width: 300,
        margin: 10,
    },
});

export default HomeScreen;