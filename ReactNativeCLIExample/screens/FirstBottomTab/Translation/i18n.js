import * as Localization from 'react-native-localize';
import i18n from 'i18n-js';

const de = {
    "title": "Titel in deutsch",
    "current": "Die Sprache ist auf \"{{language}}\" gesetzt"
}

const en = {
    "title": "Title in english",
    "current": "The current language is \"{{language}}\""
}

i18n.locale = Localization.getLocales()[0].languageCode;
i18n.fallbacks = true;
i18n.translations = { en, de };

export default i18n;