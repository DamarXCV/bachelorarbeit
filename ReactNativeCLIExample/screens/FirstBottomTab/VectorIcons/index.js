import React from "react";
import { View, Text, StyleSheet } from "react-native";

import Entypo from 'react-native-vector-icons/Entypo'
import Ionicons from 'react-native-vector-icons/Ionicons'
import EvilIcons from 'react-native-vector-icons/EvilIcons'

class VectorIcons extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                <Text style={styles.header}>VectorIcons Screen</Text>
                <Ionicons name="md-checkmark-circle" size={32} color="green" />
                <Entypo name="bug" size={32} color="blue" />
                <EvilIcons name="search" size={32} color="red" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 24,
    },
});

export default VectorIcons;